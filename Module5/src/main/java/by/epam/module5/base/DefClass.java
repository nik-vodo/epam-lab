package by.epam.module5.base;

import java.util.Objects;

//for class only public and package-private(default)
class DefClass {
    private String value;

    public DefClass() {
        value = "def class";
    }

    public DefClass(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefClass defClass = (DefClass) o;
        return getValue().equals(defClass.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "value='" + value + '\'' +
                '}';
    }
}
