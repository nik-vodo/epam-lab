package by.epam.module5.base;

public final class MyMath {
    public static final int MY_CONST = 22;

    public static int abs(int value) {
        if (value >= 0) {
            return value;
        } else {
            return -value;
        }
    }
}
