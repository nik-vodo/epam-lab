package by.epam.module5.base;

import java.util.Objects;

public class PublicClass {
    private String value;

    public PublicClass() {
        value = "public class";
    }

    public PublicClass(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PublicClass that = (PublicClass) o;
        return getValue().equals(that.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }

    public void useDefClass() {
        DefClass val = new DefClass();
        System.out.println(val.getValue() + value);
    }


    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " {" +
                "value='" + value + '\'' +
                '}';
    }
}
