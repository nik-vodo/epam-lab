package by.epam.module5.main;


import by.epam.module5.base.Dog;
//import by.epam.module5.new_base.Dog;

public class Main {
    public static void main(String[] args) {
        Dog dog1 = new Dog("Funtik", 2);
        System.out.println("Dog class full name from class : " + Dog.class.getName());
        System.out.println("Dog class full name from dog1 object : " + dog1.getClass().getName());

        by.epam.module5.new_base.Dog dog2 = new by.epam.module5.new_base.Dog("Funtik", 2,"terier");
    }
}
