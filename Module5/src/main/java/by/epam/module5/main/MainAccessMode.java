package by.epam.module5.main;

//import by.epam.module5.base.DefClass;

import by.epam.module5.base.PublicClass;

public class MainAccessMode {
    public static void main(String[] args) {
        PublicClass publicClass = new PublicClass();
        //DefClass defClass=new DefClass();

        publicClass.useDefClass();
    }
}
