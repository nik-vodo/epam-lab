package by.epam.module5.main;

//import static by.epam.module5.base.MyMath.MY_CONST;
//import static by.epam.module5.base.MyMath.abs;
import static by.epam.module5.base.MyMath.*;

public class MainStatic {
    public static void main(String[] args) {
        int someCalc = MY_CONST - 50;
        someCalc = abs(someCalc);

        System.out.println(someCalc);

        //use implicit import
        String foo="";
    }
}
