package by.epam;

import by.epam.module6.block.BlockClass;
import by.epam.module6.loop.*;
import by.epam.module6.simple_switch.SwitchFallThrough;
import by.epam.module6.simple_switch.SwitchSimple;

public class App {
    public static void main(String[] args) {

        BlockClass blockClass = new BlockClass();
        blockClass.doSome();

        Loop loop = new Loop();
        System.out.println("numbers from loop: " + loop.numbers());

        loop.multipleInit();
        loop.multipleInitTermin(3);

        LoopForEach loopForEach = new LoopForEach();
        loopForEach.arrayForEach();
        loopForEach.listForEach();
        loopForEach.enumForEach();

        LoopWhile loopWhile = new LoopWhile();
        loopWhile.simpleWhile();
        loopWhile.simpleDoWhile();
        loopWhile.simpleWhileWithBreak(2);
        loopWhile.simpleWhileWithContinue(2);

        LoopNested loopNested = new LoopNested();
        loopNested.nestedLoopWithBreak();
        loopNested.nestedLoopWithContinue();
        loopNested.nestedLoopWithBreakAndLabel();
        loopNested.nestedLoopWithContinueAndLabel();

        SwitchSimple switchSimple = new SwitchSimple();
        switchSimple.simpleNumberSwitch(2);
        switchSimple.simpleStringSwitch("banana");
        switchSimple.simpleEnumSwitch(LoopForEach.Months.APRIL);

        SwitchFallThrough switchFallThrough = new SwitchFallThrough();
        switchFallThrough.fallThroughSwitch(LoopForEach.Months.AUGUST);

        LoopReturn loopReturn = new LoopReturn();
        loopReturn.loopReturnWithoutValue(2);
        String returned = loopReturn.loopReturnWithValue(2);
        System.out.println(returned);

    }
}
