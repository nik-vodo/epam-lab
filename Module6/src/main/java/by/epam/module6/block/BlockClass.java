package by.epam.module6.block;

public class BlockClass {

    public void doSome() {
        int var1;
        int var2;
        int var3;

        var1 = 10;
        var2 = 15;

        if (var1 > 5) {
            var3 = Math.min(10, 15) + 4;
        } else {
            var3 = Math.max(10, 15) + 4;
        }

        System.out.println("var3: " + var3);
    }
}
