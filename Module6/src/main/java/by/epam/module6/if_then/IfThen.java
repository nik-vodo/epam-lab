package by.epam.module6.if_then;

public class IfThen {

    public void ifThen(int val) {
        if (val < 5) {
            System.out.println(val + " is less than 5");
        } else {
            System.out.println(val + " is greater than 5");
        }
    }


    public void ifThenElse(int val) {
        if (val < 5) {
            System.out.println(val + " is less than 5");
        } else if (val<10) {
            System.out.println(val + " is greater than 4 and less than 10");
        }else{
            System.out.println(val + " is greater than 10");
        }
    }

}
