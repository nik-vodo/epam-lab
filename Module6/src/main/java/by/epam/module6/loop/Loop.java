package by.epam.module6.loop;

import java.util.ArrayList;
import java.util.List;

public class Loop {

    public List<Integer> numbers() {
        List<Integer> result;
        result = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            result.add(i);
        }
        return result;
    }

    public void multipleInit() {
        System.out.println("Loop with multiple initialization:");

        for (int i = 0, j = 10; i < 10 && j > 0; i++, j--) {
            System.out.println("i" + i + " - j" + j);
        }
    }

    public void multipleInitTermin(int terminVar) {
        System.out.println("Loop with multiple initialization and termination:");

        for (int i = 0, j = 10; i < 10 && j > 0; i++, j--) {
            if (i == terminVar) {
                break;
            }
            System.out.println("i" + i + " - j" + j);
        }
    }
}
