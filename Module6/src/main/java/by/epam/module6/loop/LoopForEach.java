package by.epam.module6.loop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoopForEach {
    public enum Months {
        JANUARY,
        FEBRUARY,
        MARCH,
        APRIL,
        MAY,
        JUNE,
        JULY,
        AUGUST,
        SEPTEMBER,
        OKTOBER,
        NOVEMBER,
        DECEMBER,
    }

    public void arrayForEach() {
        int[] arr = {5, 8, 14, 22, 34, 6};

        System.out.println("foreach loop on array:");
        for (int ar : arr) {
            System.out.println(ar);
        }
    }

    public void listForEach() {
        List<Integer> list;
        list = new ArrayList<>(Arrays.asList(8, 12, 1, 0, 7, 9, 6));

        System.out.println("foreach loop on list:");

        for (Integer l : list) {
            System.out.println(l);
        }
    }

    public void enumForEach() {
        System.out.println("foreach loop on enum:");

        for (Months m : Months.values()) {
            System.out.println(m);
        }
    }
}
