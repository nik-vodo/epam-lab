package by.epam.module6.loop;

public class LoopInfinite {

    public void infiniteWhile1() {
        int i;
        i = 1;

        System.out.println("infinite while 1:");

        while (i == 1) {
            System.out.println("gip-gip ura!!! ");
        }
    }

    public void infiniteWhile2() {
        System.out.println("infinite while 2:");

        while (true) {
            System.out.println("gip-gip ura!!! ");
        }
    }
}
