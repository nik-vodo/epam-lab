package by.epam.module6.loop;

public class LoopNested {

    public void nestedLoopWithBreak() {
        System.out.println("nested loop with break:");

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (j == 3) {
                    System.out.println("in break block");
                    break;
                }
                System.out.println("i=" + i + " - j=" + j);
            }
        }
    }

    public void nestedLoopWithBreakAndLabel() {
        System.out.println("nested loop with break and label:");

        label:
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (j == 3) {
                    System.out.println("in break block");
                    break label;
                }
                System.out.println("i=" + i + " - j=" + j);
            }
        }
    }

    public void nestedLoopWithContinue() {
        System.out.println("nested loop with continue:");

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (j == 3) {
                    System.out.println("in continue block");
                    continue;
                }
                System.out.println("i=" + i + " - j=" + j);
            }
        }
    }

    public void nestedLoopWithContinueAndLabel() {
        System.out.println("nested loop with continue and label:");

        label:
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (j == 3) {
                    System.out.println("in continue block");
                    continue label;
                }
                System.out.println("i=" + i + " - j=" + j);
            }
        }
    }
}
