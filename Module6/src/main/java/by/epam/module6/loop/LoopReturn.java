package by.epam.module6.loop;

public class LoopReturn {

    public void loopReturnWithoutValue(int terminVar) {
        System.out.println("Loop with return without value:");

        for (int i = 0; i < 10; i++) {
            if (i == terminVar) {
                return;
            }
            System.out.println("i" + i);
        }
    }

    public String loopReturnWithValue(int conditionVal) {
        System.out.println("Loop with return without value:");
        int result;

        for (int i = 0; i < 10; i++) {
            if (i == conditionVal) {
                result = conditionVal * 2;
                return "return doubled conditionVal: " + result;
            }
            System.out.println("i" + i);
        }

        return "conditionVal not in the loop range  ";
    }
}
