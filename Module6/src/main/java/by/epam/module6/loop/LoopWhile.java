package by.epam.module6.loop;

public class LoopWhile {
    public void simpleWhile() {
        int i;
        i = 0;

        System.out.println("simple while:");

        while (i < 5) {
            System.out.println("i: " + i);
            i++;
        }
    }

    public void simpleDoWhile() {
        int i;
        //i = 0;
        i = 10;

        System.out.println("simple do while:");

        do {
            System.out.println("i: " + i);
        } while (++i < 5);
    }

    public void simpleWhileWithBreak(int breakVar) {
        int i;
        i = 0;

        System.out.println("simple while with break:");

        do {
            if (i == breakVar) {
                System.out.println("in break block");
                break;
            }
            System.out.println("i: " + i);
        } while (++i < 5);
    }

    public void simpleWhileWithContinue(int breakVar) {
        int i;
        i = 0;

        System.out.println("simple while with continue:");

        do {
            if (i == breakVar) {
                System.out.println("in continue block");
                continue;
            }
            System.out.println("i: " + i);

        } while (++i < 5);
    }
}
