package by.epam.module6.simple_switch;

import by.epam.module6.loop.LoopForEach;

public class SwitchFallThrough {
    public void fallThroughSwitch(LoopForEach.Months month) {
        System.out.println("fall-through switch:");

        switch (month) {
            case DECEMBER:
            case JANUARY: {
            }
            case FEBRUARY: {
                System.out.println(month + " is an winter month");
                break;
            }
            case MARCH:
            case APRIL:
            case MAY:
                System.out.println(month + " is a spring month");
                break;
            case JUNE:
            case JULY:
            case AUGUST:
                System.out.println(month + " is a summer month");
                break;
            case SEPTEMBER:
            case OKTOBER:
            case NOVEMBER:
                System.out.println(month + " is an autumn month");
                break;
            default:
                System.out.println("no such month case");
        }
        System.out.println();
    }
}
