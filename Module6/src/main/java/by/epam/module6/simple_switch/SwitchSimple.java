package by.epam.module6.simple_switch;

import by.epam.module6.loop.LoopForEach;

public class SwitchSimple {

    public void simpleNumberSwitch(int switcher) {
        System.out.println("switch on numbers:");

        switch (switcher) {
            case 1: {
                System.out.println("case1");
                break;
            }
            case 2: {
                System.out.println("case2");
                break;
            }
            case 3:
                System.out.println("case3");
                break;
            default:
                System.out.println("default case");
        }

        System.out.println();
    }

    public void simpleStringSwitch(String switcher) {
        System.out.println("switch on numbers:");

        if (switcher == null) {
            System.out.println("switcher is not correct");
        } else {
            switch (switcher) {
                case "banana": {
                    System.out.println("banana case");
                    break;
                }
                case "mango": {
                    System.out.println("mango case");
                    break;
                }
                case "Grape":
                    System.out.println("Grape case");
                    break;
                default:
                    System.out.println("default case");
            }
        }
        System.out.println();
    }


    public void simpleEnumSwitch(LoopForEach.Months month) {
        System.out.println("switch on enum:");

        switch (month) {
            case JANUARY: {
                System.out.println("January case");
                break;
            }
            case FEBRUARY: {
                System.out.println("February case");
                break;
            }
            case MARCH:
                System.out.println("March case");
                break;
            case APRIL:
                System.out.println("April case");
                break;
            case MAY:
                System.out.println("May case");
                break;
            case JUNE:
                System.out.println("June case");
                break;
            case JULY:
                System.out.println("July case");
                break;
            case AUGUST:
                System.out.println("August case");
                break;
            case SEPTEMBER:
                System.out.println("September case");
                break;
            case OKTOBER:
                System.out.println("Oktober case");
                break;
            case NOVEMBER:
                System.out.println("November case");
                break;
            case DECEMBER:
                System.out.println("December case");
                break;
            default:
                System.out.println("no such month case");
        }
        System.out.println();
    }
}

