package by.epam.module7.big;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalComparisonDemo {
    private BigDecimal bigDec1;
    private BigDecimal bigDec2;

    public BigDecimalComparisonDemo() {
    }

    public BigDecimalComparisonDemo(BigDecimal bigDec1, BigDecimal bigDec2) {
        this.bigDec1 = bigDec1;
        this.bigDec2 = bigDec2;
    }

    public boolean equalCheck() {
        return bigDec1.equals(bigDec2);
    }

    public BigDecimal subst() {
        return bigDec1.subtract(bigDec2);
    }

    public BigDecimal mult() {
        return bigDec1.multiply(bigDec2);
    }

    public BigDecimal div() {
        return bigDec1.divide(bigDec2, 3, RoundingMode.CEILING);
    }
}
