package by.epam.module7.big;

import java.math.BigInteger;

public class BigIntegerDemo {
    private BigInteger bidInt1;
    private BigInteger bidInt2;

    public BigIntegerDemo() {
    }

    public BigIntegerDemo(BigInteger bidInt1, BigInteger bidInt2) {
        this.bidInt1 = bidInt1;
        this.bidInt2 = bidInt2;
    }

    public BigInteger addition() {
        return bidInt1.add(bidInt2);
    }

    public BigInteger subst() {
        return bidInt1.subtract(bidInt2);
    }

    public BigInteger mult() {
        return bidInt1.multiply(bidInt2);
    }

    public BigInteger div() {
        return bidInt1.divide(bidInt2);
    }
}
