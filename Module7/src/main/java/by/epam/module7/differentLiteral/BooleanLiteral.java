package by.epam.module7.differentLiteral;

public class BooleanLiteral {

    public void converter() {
        boolean primBool;
        Boolean wrapBool;

        primBool = true;
        wrapBool = false;

        //to string conversion
        String primBoolString = String.valueOf(primBool);
        String primBoolString1 = Boolean.toString(primBool);
        String wrapBoolString = wrapBool.toString();

        System.out.println("primBool to string=" + primBoolString);
        System.out.println("wrapBool to string=" + wrapBoolString);

        //to boolean conversion

        primBool = Boolean.parseBoolean("false");
        wrapBool = Boolean.valueOf("true");
    }
}
