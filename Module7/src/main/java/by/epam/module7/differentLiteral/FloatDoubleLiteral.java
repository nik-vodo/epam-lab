package by.epam.module7.differentLiteral;

public class FloatDoubleLiteral {
    float float1;
    double double1;

    public void showStandardNotation() {
        double1 = 155.6;
        float1 = 155.6f;

        System.out.printf("\nStandard notation:%ndouble: %.1f%nfloat %.1f%n", double1, float1);
    }

    public void showScientificNotation() {
        double1 = 1.556e2;
        float1 = 1.556e2f;

        System.out.printf("Scientific notation:%ndouble: %.3e%nfloat %.3e%n", double1, float1);
    }
}
