package by.epam.module7.differentLiteral;

public class FloatLiteral {

    public void converter() {
        float primFloat;
        Float wrapFloat;

        primFloat = 12.56f;
        wrapFloat = 12.56f; //boxing
        //wrapFloat=Float.valueOf(primFloat);

        //to string conversion
        String primFloatString = String.valueOf(primFloat);
        String primFloatString1 = Float.toString(primFloat);
        String wrapFloatString = wrapFloat.toString();

        System.out.println("primFloat=" + primFloatString);
        System.out.println("wrapFloat=" + wrapFloatString);

        //from string conversion
        primFloat = Float.parseFloat("145.78");
        wrapFloat = Float.valueOf("145.78");
    }
}
