package by.epam.module7.differentLiteral;

public class IntegerLiteral {

    public void converter() {
        int primIntDecimal;
        int primIntHex;

        Integer wrapIntDecimal;
        Integer wrapIntHex;

        String primIntDecimalStr;
        String primIntHexStr;
        String wrapIntDecimalStr;
        String wrapIntHexStr;

        //from String
        primIntDecimal = Integer.parseInt("15");
        primIntHex = Integer.parseInt("1F4", 16);

        wrapIntDecimal = Integer.valueOf("30");
        wrapIntHex = Integer.valueOf("1F5", 16);

        //to String
        primIntDecimalStr = String.valueOf(primIntDecimal);
        primIntHexStr = Integer.toHexString(primIntHex);

        wrapIntDecimalStr = Integer.toString(wrapIntDecimal);
        //wrapIntHexStr = Integer.toHexString(wrapIntHex);
        wrapIntHexStr = Integer.toString(wrapIntHex, 16);

        System.out.println("\nto string conversion:");
        System.out.println("primIntDecimal=" + primIntDecimalStr);
        System.out.println("primIntHex=" + primIntHexStr);
        System.out.println("wrapIntDecimal=" + wrapIntDecimalStr);
        System.out.println("wrapIntHex=" + wrapIntHexStr);

    }

    public void constructorUsage() {
        int someInt;
        String someNumericalStr;
        Integer integer1, integer2;

        someInt = 10;
        someNumericalStr = "128";

        //integer1 = new Integer(someInt);  //deprecated since 9- unnecessary boxing
        //integer2 = new Integer(someNumericalStr); // deprecated since 9

        //System.out.println(integer1 + " , " + integer2);
    }

    public void valueOfUsage() {
        int someInt;
        String someNumericalStr;
        String someNumericalHexStr;

        Integer integer1, integer2, integer3;

        someInt = 10;
        someNumericalStr = "128";
        someNumericalHexStr = "1A";  //not "0x1A"

        integer1 = Integer.valueOf(someInt);
        integer2 = Integer.valueOf(someNumericalStr);
        integer3 = Integer.valueOf(someNumericalHexStr, 16);

        System.out.println("\nInteger.valueOf:\nint value: " + integer1 + ",\nnumerical string:  " + integer2 + ",\n numerical hex string: " + integer3);
    }
}
