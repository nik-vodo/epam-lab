package by.epam.module7.funk;

public class Boxing {

    public void manualBoxing() {
        int int1;
        Integer integer;
        integer = 10;

        int1 = integer.intValue();  //unnecessary
        integer = Integer.valueOf(20); //unnecessary
    }

    public void unboxing() {
        //no need to call a method such as intValue( ) or doubleValue( ).
        int int1, int2;
        Integer integer1, integer2;

        integer1 = 10;
        int1 = integer1;

        //unbox-increment-autobox back
        ++integer1;
        integer2 = integer1 + 5;
        int2 = integer1 + 5;
    }

    //can pass int - autoboxing happens
    public void autoBoxing(Integer value) {
        //no need explicitly construct object
        Integer integer1 = 15;
        Integer integer2 = value;
    }
}
