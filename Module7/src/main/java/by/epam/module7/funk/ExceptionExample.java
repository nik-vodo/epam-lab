package by.epam.module7.funk;

public class ExceptionExample {

    public void divException() {
        System.out.println("\nDivision Exception example:");
        int[] array = {5, 8, 0, 7};
        try {
            double result;
            for (int i = 0; i < array.length; i++) {
                result = 10 / array[i];
                System.out.printf("10/%d=%.2f%n", array[i], result);
            }
        } catch (ArithmeticException ae) {
            System.out.println("ArithmeticException: division by 0");
        }
    }

    public void moduloException() {

        int[] array = {5, 8, 0, 7};
        try {
            double result;
            for (int i = 0; i < array.length; i++) {
                result = 10 % array[i];
                System.out.printf("10/%%%d = %.2f %n", array[i], result);
            }
        } catch (ArithmeticException ae) {
            System.out.println("ArithmeticException: modulo  0");
        }
    }
}
