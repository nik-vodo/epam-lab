package by.epam.module7.funk;

public class FloatFormatting {

    public void systemOutFormat(float float1) {
        System.out.format("\nsystem.out with Format1: you enter %10.3f.%n", float1);
        System.out.format("system.out with Format2: you enter %-10.3f.%n", float1);
    }

    public void printfFormat(float float1) {
        System.out.printf("printf with Format1: you enter %10.3f.%n", float1);
        System.out.printf("printf with Format2: you enter %-10.3f.%n", float1);
    }
}
