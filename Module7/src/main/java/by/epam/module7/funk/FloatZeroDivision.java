package by.epam.module7.funk;

public class FloatZeroDivision {

    public float doIntFloatDivision(int int1, float float1) {
        return int1 / float1;
    }

    public float doFloatFloatDivision(float float1, float float2) {
        return float2 / float1;
    }
}
