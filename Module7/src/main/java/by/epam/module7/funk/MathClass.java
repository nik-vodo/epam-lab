package by.epam.module7.funk;

import java.util.Arrays;

public class MathClass {

    public void useMathClassForInt() {
        int[] data = {10, 20, -6, 11, 25, -19};
        int minValue, maxValue;
        int[] absData;

        absData = new int[data.length];
        minValue = maxValue = data[0];

        for (int i = 1; i < data.length; i++) {
            minValue = Math.min(minValue, data[i]);
            maxValue = Math.max(maxValue, data[i]);
            absData[i] = Math.abs(data[i]);
        }

        System.out.println("Initial array: " + Arrays.toString(data));
        System.out.printf("Min value=%d, max value=%d%n", minValue, maxValue);
        System.out.println("Absolut value array: " + Arrays.toString(absData));
    }
}
