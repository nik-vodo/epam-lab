package by.epam.module7.funk;

public class OverUnderFlow {

    public void overflow(){
        byte byte1;
        int int1,int2;

        byte1=(byte)129;
        int1=2_147_483_647;
        int2=int1+2;

        System.out.println("result of (byte)129: "+byte1);
        System.out.println("result of int1=2_147_483_647;  int2=int1+2; :" +int2);
    }

    public void underflow(){
        float float1;

        float1=0.00_000_000_001f;

        System.out.println("float of 0.00_000_000_001f: "+float1);
    }
}
