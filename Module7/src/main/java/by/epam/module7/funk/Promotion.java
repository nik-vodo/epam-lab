package by.epam.module7.funk;

public class Promotion {

    public void promotionDemo() {
        byte b;
        char c;
        int i;
        float f;
        double d;

        b = 2;
        c = '\u0001';
        i = 10;
        f = 4.5f;
        d = 20.2;

        int[] array = new int[b];
        array[0] = c;

        System.out.println("\nPromotion example:");
        System.out.println("c = '\\u0001'; array[0] = c; array[0]=" + array[0]);
        System.out.printf("i = 10; f = 4.5f; d = 20.2; result of int*float+double: %4.4f%n", (i * f + d));
    }
}
