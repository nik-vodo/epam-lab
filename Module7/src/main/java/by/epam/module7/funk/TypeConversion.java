package by.epam.module7.funk;

public class TypeConversion {

    public void showTypeConversion() {
        byte byte1;
        int int1;
        long long1;
        float float1;
        double double1;

        //narrowing(explisit)
        int1 = (int) 14.5f;
        byte1 = (byte) 14.5f;

        System.out.println("\nnarrowing conversion of 14.5f:\n to int: " + int1 + "\nto byte: " + byte1);

        //widening(implicit)
        long1 = int1;
        float1 = int1;
        double1 = Math.sqrt(float1);

        System.out.println("widening conversion of int " + int1 + ":\n to long: " + long1 + "\nto float: " + float1);
        System.out.println("widening conversion of float" + float1 + " to double: " + double1);
    }
}
