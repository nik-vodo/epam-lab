package by.epam.module7.init;

public class LiteralInit {

    public void init() {
        int intLit;
        long longLit;
        boolean bool;
        float floatLit;
        double doubleLit;
        char charLit;

        //initialize with decimal
        intLit = 10;
        longLit=10L;
        floatLit=2.0f;
        doubleLit=3.5;
        charLit='g';


        //initialize with hexadecimal
        intLit=0x2F;
        longLit=0x2FL;
        floatLit=0x2F;
        doubleLit=0x2F;

        //initialization with boolean
        bool=true;

        //initialization with underscore (don`t displayed)
        intLit=10_350;
        longLit=15__125__100L;
        floatLit=1_120.05f;
        doubleLit=120_136_248.124_256;

        /* error examples
        double d1 = _10000;
        double d2 = 1_00_000_;
        double d3 = 1_00_000._89;
        double d4 = 1_00_000_.89;
        double d5 = 1_00_000_._89;
        float f1 = 1_00_000_.89;
        double d6 = 1_00_000_L;
        double d7 = 1_00_000L_;
        double d8 = 100000_L;
        double d9 = 1_00_000_F;
        double d10 = 100_000D_;
        // hexadecimal
        int n1 = _0xA123;
        int n2 = 0_xA123;
        int n3 = 0x_A123;
        // binary
        int n4 = _0B1010;
        int n5 = 0_B1010;
        int n6 = 0B_1010;          */





    }
}
