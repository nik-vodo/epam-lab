package by.epam.module7.main;

import by.epam.module7.differentLiteral.BooleanLiteral;
import by.epam.module7.differentLiteral.FloatDoubleLiteral;
import by.epam.module7.differentLiteral.IntegerLiteral;
import by.epam.module7.funk.*;
import by.epam.module7.minmax.MinMaxIntegralWrapperValue;

public class Main {
    public static void main(String[] args) {
        BooleanLiteral booleanLiteral = new BooleanLiteral();
        booleanLiteral.converter();

        MinMaxIntegralWrapperValue minMaxIntegralWrapperValue = new MinMaxIntegralWrapperValue();
        minMaxIntegralWrapperValue.showMinMax();

        IntegerLiteral integerLiteral = new IntegerLiteral();
        integerLiteral.converter();
        integerLiteral.constructorUsage();
        integerLiteral.valueOfUsage();

        OverUnderFlow overUnderFlow = new OverUnderFlow();
        overUnderFlow.overflow();

        ExceptionExample exceptionExample = new ExceptionExample();
        exceptionExample.divException();

        TypeConversion typeConversion = new TypeConversion();
        typeConversion.showTypeConversion();

        FloatDoubleLiteral floatDoubleLiteral = new FloatDoubleLiteral();
        floatDoubleLiteral.showStandardNotation();
        floatDoubleLiteral.showScientificNotation();

        FloatFormatting floatFormatting = new FloatFormatting();
        floatFormatting.systemOutFormat(34.2f);
        floatFormatting.printfFormat(34.2f);

        Promotion promotion = new Promotion();
        promotion.promotionDemo();
    }
}
