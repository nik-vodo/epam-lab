package by.epam.module7.minmax;

public class MinMaxFloatPointWrapperValue {

    public void showMinMax() {
        System.out.println("Float max value: " + Float.MAX_VALUE);
        System.out.println("Float min value: " + Float.MIN_VALUE);

        System.out.println("Double max value: " + Double.MAX_VALUE);
        System.out.println("Double min value: " + Double.MIN_VALUE);

        System.out.println("Float positive infinity : " + Float.POSITIVE_INFINITY);
        System.out.println("Float negative infinity: " + Float.NEGATIVE_INFINITY);
        System.out.println("Float NAN: " + Float.NaN);

        System.out.println("Double positive infinity: " + Double.POSITIVE_INFINITY);
        System.out.println("Double negative infinity: " + Double.NEGATIVE_INFINITY);
        System.out.println("Double NAN: " + Double.NaN);
    }
}
