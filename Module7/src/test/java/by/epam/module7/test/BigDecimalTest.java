package by.epam.module7.test;

import by.epam.module7.big.BigDecimalComparisonDemo;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BigDecimalTest {
    BigDecimal bigDec1;
    BigDecimal bigDec2;
    BigDecimal bigDec3;
    BigDecimalComparisonDemo bigDecDemo1;
    BigDecimalComparisonDemo bigDecDemo2;

    @BeforeAll
    void setUp() {
        bigDec1 = BigDecimal.valueOf(2.5);
        bigDec2 = BigDecimal.valueOf(2.50);
        bigDec3 = BigDecimal.valueOf(2.5);
        bigDecDemo1 = new BigDecimalComparisonDemo(bigDec1, bigDec2);
        bigDecDemo2 = new BigDecimalComparisonDemo(bigDec1, bigDec3);
    }

    @Test
    @DisplayName("Test for BigDecimal with equals")
    public void bigDecimalEqualsTest() {
        assertEquals(false, bigDecDemo1.equalCheck());
        assertEquals(true, bigDecDemo2.equalCheck());
    }

    @Test
    @DisplayName("Test for BigDecimal with compareTo")
    public void bigDecimalCompareToTest() {
        assertEquals(true, bigDec1.compareTo(bigDec2));
        assertEquals(true, bigDec1.compareTo(bigDec3));
    }

}
