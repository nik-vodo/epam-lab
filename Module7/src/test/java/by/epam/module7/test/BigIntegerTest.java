package by.epam.module7.test;

import by.epam.module7.big.BigIntegerDemo;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BigIntegerTest {
    BigInteger bigInteger1;
    BigInteger bigInteger2;
    BigIntegerDemo bigIntegerDemo;

    @BeforeAll
    void setUp() {
        bigInteger1 = BigInteger.valueOf(100);
        bigInteger2 = BigInteger.valueOf(200);
        bigIntegerDemo = new BigIntegerDemo(bigInteger1, bigInteger2);
    }

    @Test
    @DisplayName("Test for BigInteger addition")
    public void bigIntegerAdditionTest() {
        BigInteger result = BigInteger.valueOf(300);
        assertEquals(result, bigIntegerDemo.addition());
    }

    @Test
    @DisplayName("Test for BigInteger substraction")
    public void bigIntegerSubstractionTest() {
        BigInteger result = BigInteger.valueOf(-100);
        assertEquals(result, bigIntegerDemo.subst());
    }

    @Test
    @DisplayName("Test for BigInteger multiplication")
    public void bigIntegerMultiplicationTest() {
        BigInteger result = BigInteger.valueOf(20000);
        assertEquals(result, bigIntegerDemo.mult());
    }

    @Test
    @DisplayName("Test for BigInteger division")
    public void bigIntegerDivisionTest() {
        BigInteger result1 = BigInteger.valueOf(0);
        assertEquals(result1, bigIntegerDemo.div());
    }

    @Test
    @DisplayName("Test for BigInteger division by zero exception")
    public void bigIntegerDivisionByZeroTest() {
        BigIntegerDemo bigIntegerDemo1;
        bigIntegerDemo1 = new BigIntegerDemo(bigInteger1, BigInteger.ZERO);

        BigInteger result1 = BigInteger.valueOf(0);
        assertEquals(ArithmeticException.class, bigIntegerDemo1.div());
    }
}
