package by.epam.module7.test;

import by.epam.module7.funk.FloatZeroDivision;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FloatZeroDivisionTest {
    private FloatZeroDivision floatZeroDivision;
    int int1;
    float float1, float2;

    @BeforeAll
    void setUp() {
        floatZeroDivision = new FloatZeroDivision();

        int1 = 140;
        float1 = 0.0f;
        float2 = 140.2f;
    }

    @Test
    @DisplayName("Test for int/0.0f division")
    public void integerZeroFloatDivisionTest() {
        assertEquals(Float.NaN, int1 / float1);
    }

    @Test
    @DisplayName("Test for float/0.0f division")
    public void floatZeroFloatDivisionTest() {
        assertEquals(Float.NaN, float2 / float1);
    }

    @Test
    @DisplayName("Test for 0.0f/0.0f division")
    public void zeroFloatZeroFloatDivisionTest() {
        assertEquals(Float.NaN, float1 / float1);
    }
}
