package by.epam.module7.test;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PrimitiveDefaultValuesTest {
    private byte actualByte;
    private short actualShort;
    private int actualInt;
    private long actualLong;

    private float actualFloat;
    private double actualDouble;

    private boolean actualBoolean;
    private char actualChar;

    @Test
    public void testByte() {
        byte expectedByte = (byte) 0;

        assertEquals(expectedByte, actualByte);
    }

    @Test
    public void testShort() {
        short expectedShort = (short) 0;

        assertEquals(expectedShort, actualShort);
    }

    @Test
    public void testInt() {
        int expectedInt = 0;

        assertEquals(expectedInt, actualInt);
    }


    @Test
    public void testLong() {
        long expectedLong = 0L;

        assertEquals(expectedLong, actualLong);
    }

    @Test
    public void testFloat() {
        float expectedFloat = 0.0f;

        assertEquals(expectedFloat, actualFloat);
    }

    @Test
    public void testDouble() {
        double expectedDouble = 0.0d;

        assertEquals(expectedDouble, actualDouble);
    }

    @Test
    public void testBoolean() {
        boolean expectedBoolean = false;

        assertEquals(expectedBoolean, actualBoolean);
    }

    @Test
    public void testChar() {
        char expectedChar = '\u0000';

        assertEquals(expectedChar, actualChar);
    }
}
